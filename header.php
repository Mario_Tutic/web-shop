            <!--novi header s email,broj, lokacija-->
            <header>
                <div id="top-header" class="container-fluid">
                    <div class="row">
                        <div class="col-sm-8">
                            <ul class="header-links pull-left">
                                <li>
                                    <a href="index.php">
                                        <i class="fa fa-home" aria-hidden="true"></i>HOME
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-phone"></i>+021-95-51-87
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-envelope-o"></i>email@email.com
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-map-marker"></i>Ul. Kneza Trpimira 2b, 31000, Osijek
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <ul class="header-links float-end">
                                <li>
                                    <?php if (isset($_SESSION['success'])){ ?>
                                        Welcome: <?php echo $_SESSION['username'];  ?>
                                        <a href="index.php?logout='1'"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                    <?php }else{ ?>
                                        <a href="login.php">
                                            <i class="fas fa-user-alt"></i>Login
                                        </a>
                                    <?php } ?>
                                </li>
                                <li>
                                    <a href="cart.php">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

    <div class="container-fluid">
        <div class="text-center p-5">
          <h1 class="fw-bold">WebShop</h1>      
        </div>      
      </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="search">
                            <i class="fa fa-search"></i>
                            <form action="products.php" method="POST"> 
                                <input type="text" class="form-control" placeholder="Search?" name="search"> 
                                <button class="btn btn-primary" type="submit" >Search</button> </div>
                            </form>
                            
                    </div>
                </div>
            </div>
        </header>