<?php 
  session_start(); 

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="footer.css">
    <title>HomePage</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://kit.fontawesome.com/df79a87221.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

</head>
<body>
    <?php
        include ("header.php");
    ?>
        <div class="card-group">
            <form class="card" action="products.php" method="POST" id="laptops">
                <input type="hidden" name="category" value="laptop" >
                <div class="hover01 m-2">
                    <a href="" onclick="document.getElementById('laptops').submit(); return false;">
                        <figure><img src="https://preview.colorlib.com/theme/electro/img/xshop01.png.pagespeed.ic.YgJRjyP3IB.webp" class="card-img-top " alt="..."></figure>
                        <div class="card-body">
                            <h5 class="card-title">Laptop Collection</h5>
                        </div>
                    </a>
                </div>
            </form>
            
            <form class="card" action="products.php" method="POST" id="accessories">
                <input type="hidden" name="category" value="accessories">
                <div class="hover01 m-2">
                    <a href="" onclick="document.getElementById('accessories').submit(); return false;">
                        <figure><img src="https://preview.colorlib.com/theme/electro/img/xshop03.png.pagespeed.ic.7lsQL1UJA9.webp" class="card-img-top" alt="..."></figure>
                        <div class="card-body">
                            <h5 class="card-title">Accessories Collection</h5>
                        </div>
                    </a>
                </div>
            </form>

            <form class="card" action="products.php" method="POST" id="cameras">
                <input type="hidden" name="category" value="camera">
                <div class="hover01 m-2">
                    <a href="" onclick="document.getElementById('cameras').submit(); return false;">
                        <figure><img src="https://preview.colorlib.com/theme/electro/img/xshop02.png.pagespeed.ic.JMo4guOhuU.webp" class="card-img-top" alt="..."></figure>
                        <div class="card-body">
                            <h5 class="card-title">Cameras Collection</h5>
                        </div>
                    </a>
                </div>
            </form>
        </div>

    <?php
        include ("footer.php");
    ?>

          
      
    </body>
</html>