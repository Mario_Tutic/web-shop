<footer id="footer">
              <div>
                  <div class="container">
                      <div class="row">
                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h1 class="footer-title">
                                    ABOUT US
                                </h1>
                                <ul>
                                    <li>
                                        
                                    </li>
                                </ul>    
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h1 class="footer-title">
                                    CATEGORIES
                                </h1>
                                <ul class="footer-links">
                                    <li>
                                        <a href="#">Laptops</a>
                                    </li>
                                    <li>
                                        <a href="#">Cameras</a>
                                    </li>
                                    <li>
                                        <a href="#">Accessories</a>
                                    </li>
                                </ul>    
                            </div>

                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h1 class="footer-title">
                                    INFORMATION
                                </h1>
                                <ul class="footer-links">
                                    <li>
                                        <a href="#">About us</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact Us</a>
                                    </li>
                                </ul>    
                            </div>

                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h1 class="footer-title">
                                    SERVICE
                                </h1>
                                <ul class="footer-links">
                                    <li>
                                        <a href="#">My Account</a>
                                    </li>
                                    <li>
                                        <a href="#">View cart</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                </ul>    
                            </div>
                        </div>
                        
                      </div>
                  </div>
              </div>
          </footer>